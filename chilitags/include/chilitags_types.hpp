#pragma once

#include <opencv2/core/core.hpp>
#include <map>

namespace chilitags
{
        /**
            The location of the detected chilitags are stored in a 4x2 matrix
            corresponding to the outside corners of its black border. The rows
            correspond to the 2D coordinates of the corners. The corners are
            consistenly stored clockwise, starting from top-left, i.e.
            \verbatim
            {    top-left.x  ,    top-left.y  ,
                 top-right.x ,    top-right.y ,
              bottom-right.x , bottom-left.y  ,
              bottom-left.x  , bottom-left.y  }
            \endverbatim
         */
        typedef cv::Matx<float, 4, 2> Quad;
        typedef std::map<int, Quad> TagCornerMap;

        enum RefinementMode {  CORNERSUBPIX = 0,
                               FAST,
                               FAST_CORNERSUBPIX,
                               ORB,
                               ORB_CORNERSUBPIX };

}
