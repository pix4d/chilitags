#pragma once

#include <opencv2/core/core.hpp>

#include <memory>

namespace chilitags
{
        /**
         * @brief The CameraModel class with internal parameters of the camera. It is
         * used to compensate the distortion of the image. By default it is perspective
         * camera without distortion
         */
        class CameraModel
        {
        public:
                /**
                 * @brief CameraModel Constructor
                 * @param settings The settings for the chilitags detection
                 */
                CameraModel() {}

                ///@brief destructor
                virtual ~CameraModel() {}

                /**
                 * @brief isFisheye if the camera is perspective of fisheye
                 * @return true if the camera is fisheye, otherwise false
                 */
                virtual bool isFisheye() const { return false; }

                /**
                 * @brief undistort undistorts a 2D image point using the internal camera model paramaters.
                 * By default the result coordinate is the same as the distored.
                 * @param p2d input distorted point
                 * @param p2u result undistored point
                 * @return true if successful
                 */
                virtual bool undistort(const cv::Point2f& p2d, cv::Point2f& p2u) const;

                /**
                 * @brief distort distorts a 2D image point using the internal camera model paramaters.
                 * By default the result coordinate is the same as the distored.
                 * @param p2u input undistored point
                 * @param p2d result distorted point
                 */
                virtual void distort(const cv::Point2f& p2u, cv::Point2f& p2d) const;

                /**
                 * @brief distortContour distorts a contour of 2D points
                 *  @param contour vector of 2D points
                 */
                virtual void distortContour(std::vector<cv::Point2f>& contour) const {}

                /**
                 * @brief distortContour undistorts a contour of 2D points
                 *  @param contour vector of 2D points
                 */
                virtual bool undistortContour(std::vector<cv::Point2f>& contour) const { return false; }
        };

        typedef std::shared_ptr<CameraModel>  RefCameraModel;

        inline bool CameraModel::undistort(const cv::Point2f& p2d, cv::Point2f& p2u) const
        {
                p2u = p2d;
                return true;
        }

        inline void CameraModel::distort(const cv::Point2f& p2u, cv::Point2f& p2d) const
        {
                p2d = p2u;
        }
}


