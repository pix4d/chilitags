include (GenerateExportHeader)

file(GLOB_RECURSE chilitags_srcs "*.cpp")
file(GLOB_RECURSE chilitags_hdrs "*.hpp")

add_library(chilitags SHARED ${chilitags_srcs} ${chilitags_hdrs})

set_target_properties(chilitags PROPERTIES DEBUG_POSTFIX "d")
target_link_libraries(chilitags PRIVATE ${OpenCV_LIBS})

target_include_directories(chilitags SYSTEM PRIVATE ${EXTERNAL_DIR}/include ${CMAKE_BINARY_DIR})

if (UNIX)
        target_compile_options(chilitags PRIVATE -fvisibility=hidden)
endif()

generate_export_header(chilitags EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/chilitags_export.h")
