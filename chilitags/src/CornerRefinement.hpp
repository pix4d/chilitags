#pragma once
#include "../include/chilitags.hpp"
#include <opencv2/core/core.hpp>
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <string>
#include <algorithm>
#include <memory>
namespace chilitags
{

///< @brief creates a square window around the point
template<typename Tp>
inline cv::Rect_<Tp> createSquareWindow( Tp x_center, Tp y_center, Tp side_length )
{

        Tp side = static_cast<float>( side_length );

        Tp top_left_x = x_center - side * 0.5f;
        Tp top_left_y = y_center - side * 0.5f;

        return cv::Rect_<Tp>( top_left_x, top_left_y, side, side );

}


///@brief Checks if the neighborhood window lies outside the image and performs the respective corrections
inline void checkInsideImageResize( const cv::Mat& inputImage, cv::Rect& window)
{

        // make sure that the window top left corner does not have negative coordinates
        window.x = MAX(window.x, 0);
        window.y = MAX(window.y, 0);

        // make sure that the window top left corner is not outside the image on the positive sides
        // otherwise a minimum 5x5 square is enforced
        window.x = MIN( window.x, inputImage.cols - 5 );
        window.y = MIN( window.y, inputImage.rows - 5 );

        // estimate the dimensions of the window
        int window_xsize = window.x + window.width;
        int window_ysize = window.y + window.height;

        // make sure that the dimensions of the window do not exceed the image ones
        window.width  = MIN( window_xsize,  inputImage.cols );
        window.height = MIN( window_ysize,  inputImage.rows );

        // absolute size of the dimensions, not relative to the coordinate system

        window.width  -= window.x;
        window.height -= window.y;


}

        namespace redetection
        {

        inline bool response_comparator(const cv::KeyPoint& p1, const cv::KeyPoint& p2) { return p1.response > p2.response; }


        typedef std::vector<cv::KeyPoint> Keypoints;

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------

        class FeatureDetector
        {
        public:
                FeatureDetector( bool refine_corners ):
                m_refine_neighborhood(),
                m_refine_corners(refine_corners)
                {}

                virtual ~FeatureDetector(){}

                virtual void set_refine_neighborhood(float proximity_ratio, float neighborhood );

                virtual bool detect( const cv::Mat& inputImage, cv::Point2f& resulting_quad ){ return true; }

                virtual bool refine( const cv::Mat& inputImage, cv::Mat_<cv::Point2f>& points );

                virtual void set_refine_neighborhood( float neigh ){ m_refine_neighborhood = neigh; }

        protected:

                inline bool check_and_extract_point( Keypoints& keypoints, cv::Point2f& resulting_point);
                inline bool try_refine_corners(const cv::Mat &inputImage, cv::Mat_<cv::Point2f> &points, const cv::Size &window_size);

                float                   m_refine_neighborhood;

        private:

                //Attributes

                bool                    m_refine_corners;

        };

        typedef std::shared_ptr<FeatureDetector> RefFeatureDetector;


        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------


        class FASTDetector : public FeatureDetector
        {
        public:
                FASTDetector( bool refine_corners ) : FeatureDetector( refine_corners ){}
                virtual ~FASTDetector(){}

                virtual void set_refine_neighborhood(float proximity_ratio, float neighborhood );

                virtual bool detect( const cv::Mat& inputImage, cv::Point2f& resulting_point) override;

        };

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------


        class ORBDetector : public FeatureDetector
        {

        public:
                ORBDetector(bool refine_corners,
                            int n_features = 10,
                            float scale_factor = 1.2f,
                            int n_levels = 4,
                            int edge_threshold = 10,
                            int first_level = 0,
                            int WTA_K = 2,
                            int score_type = cv::ORB::FAST_SCORE,
                            int patch_size = 10);

                virtual ~ORBDetector(){}

                virtual void set_refine_neighborhood( float proximity_ratio, float neighborhood );

                virtual bool detect( const cv::Mat& inputImage, cv::Point2f& resulting_point) override;

        private:
                cv::Ptr<cv::ORB> m_orb;

        };

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------

        class DetectorFactory
        {
        public:

                static RefFeatureDetector create( size_t mode )
                {

                        bool refine_corners = ( mode == 0 || mode == 2 || mode == 4 );

                        switch ( mode )
                        {
                        default:
                        case 0: // CORNERSUBPIX

                                return RefFeatureDetector( new FeatureDetector( refine_corners ) );
                                break;

                        case 1: // FAST
                        case 2: // FAST_CORNERSUBPIX

                                return RefFeatureDetector( new FASTDetector( refine_corners ) );
                                break;

                        case 3: // ORB
                        case 4: // ORB_CORNERSUBPIX

                                return RefFeatureDetector( new ORBDetector( refine_corners ) );
                                break;

                        }
                }

        };

        }



}

