/*******************************************************************************
*   Copyright 2013-2014 EPFL                                                   *
*   Copyright 2013-2014 Quentin Bonnard                                        *
*                                                                              *
*   This file is part of chilitags.                                            *
*                                                                              *
*   Chilitags is free software: you can redistribute it and/or modify          *
*   it under the terms of the Lesser GNU General Public License as             *
*   published by the Free Software Foundation, either version 3 of the         *
*   License, or (at your option) any later version.                            *
*                                                                              *
*   Chilitags is distributed in the hope that it will be useful,               *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*   GNU Lesser General Public License for more details.                        *
*                                                                              *
*   You should have received a copy of the GNU Lesser General Public License   *
*   along with Chilitags.  If not, see <http://www.gnu.org/licenses/>.         *
*******************************************************************************/

#include "FindQuads.hpp"
#include <opencv2/imgproc/imgproc.hpp>

//#define DEBUG_FindQuads
#ifdef DEBUG_FindQuads
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core_c.h> // CV_AA
#endif

namespace {

#ifdef DEBUG_FindQuads
void drawContour(cv::Mat &image, cv::Mat &contour, cv::Scalar color, cv::Point offset) {
    std::vector<std::vector<cv::Point> >contours;
    contours.push_back(contour);
    cv::drawContours(image, contours, 0, color, 1, CV_AA, cv::noArray(), INT_MAX, offset);
    float perimeter = std::abs(cv::arcLength(contour, true));
    cv::putText(image, cv::format("%.1f", perimeter), offset+contours[0][0],
                cv::FONT_HERSHEY_SIMPLEX, 1.0f, color);
}
#endif

}

namespace chilitags{
        FindQuads::FindQuads(RefCameraModel refCamera):
                mGrayPyramid(1),
                mBinaryPyramid(1),
                mMinInputWidth(160),
                mRefCamera(refCamera),
                mQuadrilateralReforming(false)

        {
        }

        void FindQuads::setQuadrilateralReforming(bool state){ mQuadrilateralReforming = state; }


        std::vector<Quad> FindQuads::operator()(const cv::Mat &greyscaleImage)
        {
                //TODO function too long, split it

                std::vector<Quad> quads;
#ifdef DEBUG_FindQuads
    cv::Mat debugImage = cv::Mat::zeros(
        cv::Size(2*greyscaleImage.cols, greyscaleImage.rows),
        CV_8UC3);
#endif

                mGrayPyramid[0] = greyscaleImage;

                // Subsample the image by a factor two,
                // as long as the width is at least mMinInputWidth
                unsigned int nPyramidLevel = 1;

                if (mMinInputWidth > 0) {
                        while (mGrayPyramid[nPyramidLevel-1].cols/2 >= mMinInputWidth) {
                                if (nPyramidLevel >= mGrayPyramid.size()) mGrayPyramid.push_back(cv::Mat());
                                cv::resize(mGrayPyramid[nPyramidLevel-1], mGrayPyramid[nPyramidLevel], cv::Size(), 0.5f, 0.5f, cv::INTER_NEAREST);
                                ++nPyramidLevel;
                        }
                }

                while(mBinaryPyramid.size() < nPyramidLevel) mBinaryPyramid.push_back(cv::Mat());

                for(unsigned int i = 0; i < nPyramidLevel; ++i) {
                        cv::Canny(mGrayPyramid[i], mBinaryPyramid[i], 100, 200, 3);
                }

                for(int i = nPyramidLevel - 1; i >= 0; --i) //starting with the lowest definition, so the highest definition are last, and can simply override the first ones.
                {
                        float scale = static_cast<float>( 1 << i );
#ifdef DEBUG_FindQuads
        cv::Point offset(debugImage.cols-2*mBinaryPyramid[i].cols,0);
        cv::rectangle(debugImage, cv::Rect(offset.x, offset.y, greyscaleImage.cols/scale, greyscaleImage.rows/scale), cv::Scalar::all(255));
#endif

                        std::vector<std::vector<cv::Point> > contours;
                        cv::findContours(mBinaryPyramid[i], contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

                        for(const auto& contour : contours)
                        {
                                if (contour.size() <= 2)
                                        continue;

                                std::vector<cv::Point_<float>> contour_f(contour.size()); // float coordinate contour

                                for (size_t i = 0; i < contour.size(); ++i)
                                {
                                        // render the coordinates float and scale them up to the original image size
                                        contour_f[i].x = static_cast<float>(contour[i].x)*scale;
                                        contour_f[i].y = static_cast<float>(contour[i].y)*scale;
                                }

                                //undistort the contour taking into account the scaling of the image. The small scaled images do not need undistortion
                                //because curved lines are approximating straight ones as resolution drops
                                bool distortion = (mRefCamera != nullptr) && mRefCamera->isFisheye() && (scale <= 4);
                                if (distortion)
                                {
                                        if (!mRefCamera->undistortContour(contour_f)) continue;
                                }

                                cv::Mat normalisedContour;
                                if (contourSelection(contour_f, mQuadrilateralReforming, 1.0))
                                {
                                        if (distortion)
                                        {
                                                mRefCamera->distortContour(contour_f);
                                        }
                                        normalisedContour = cv::Mat(contour_f);
                                }

                                //quadrilaterals only!
                                if (normalisedContour.rows == 4)
                                {
#ifdef DEBUG_FindQuads
                        drawContour(debugImage, normalisedContour, cv::Scalar(0,255,0), offset);
#endif
                                    quads.push_back(normalisedContour.reshape(1));
                                }
#ifdef DEBUG_FindQuads
                                else if(normalisedContour.rows > 1) // not quadrilaterals
                                {
                                    drawContour(debugImage, normalisedContour, cv::Scalar(0,0,255), offset);
                                }
#endif
                        }
#ifdef DEBUG_FindQuads
        cv::putText(debugImage, cv::format("%d, %d", quads.size(), contours.size()), offset+cv::Point(32,32),
                    cv::FONT_HERSHEY_SIMPLEX, 0.5f, cv::Scalar::all(255));
#endif
                }
#ifdef DEBUG_FindQuads
    cv::Mat temp;
    cv::resize(debugImage, temp, cv::Size(800, 600));
    cv::imshow("FindQuads", temp);
    cv::waitKey(0);
    cv::destroyWindow("FindQuads");
#endif

                return quads;
        }

} /* namespace chilitags */
