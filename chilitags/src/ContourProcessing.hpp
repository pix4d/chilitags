# pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <array>
#include <numeric>

namespace
{
        const double MATRIX_SIZE = 10.0;
        const double MIN_TAG_SIZE = 1.1 * MATRIX_SIZE;
        const double MIN_TAG_AREA = MIN_TAG_SIZE * MIN_TAG_SIZE;
}


namespace chilitags
{

        /** @brief Finds the indices that correspond to a sorted vector
         *
         *  @return the sorted indices
         *
         */
        template<typename Tp>
        inline std::vector< int > ordered(std::vector<Tp> const& values);


        /** @brief Reshapes an almost quadrilateral contour into a quadrilateral one by finding the biggest 4 edges and
         *         by finding their intersections.
         *
         *  @param contour The vector of 2D points
         *
         *  @warning opencv functions fail under double type. Choose float instead.
         *
         */
        template<typename Tp>
        inline bool makeQuadrilateral(std::vector<cv::Point_<Tp>>& contour);


        /** @brief Polynomial approximation of contour and convex hull.
         *
         *  @param contour The vector of 2D points
         *
         *  @param perimeter The perimeter of the contour
         *
         */
        template<typename Tp>
        void normaliseContour(std::vector<cv::Point_<Tp>>& contour, double perimeter);


        /** @brief Calculates the intersection between two lines
         *
         *  @param L1 two 2D point array (corners of the side of the shape)
         *
         *  @param L2 second 2D array
         *
         *  @param pt resulting intersection
         *
         */
        template<typename Tp>
        inline bool computeIntersect(const std::array<cv::Point_<Tp>,2>& L1,
                                     const std::array<cv::Point_<Tp>,2>& L2,
                                     cv::Point_<Tp>& pt);

        /** @brief Filters the input contour based on a variety of criteria.
         *  @return True if the contour passed the filtering, which means that
         *  it is possible that it representes a visual marker
         */
        template<typename Tp>
        bool inline contourSelection(std::vector<cv::Point_<Tp>>& contour);



        // implementations ----------------------------------------------------------------------------------------------------------------


        template<typename Tp>
        inline std::vector< int > ordered(std::vector<Tp> const& values)
        {
                std::vector< int > indices(values.size());

                std::iota(begin(indices), end(indices), static_cast<int>(0));

                std::sort(begin(indices), end(indices),
                          [&](int a, int b) { return values[a] > values[b]; }
                          );
                return indices;
        }

        template<typename Tp>
        void normaliseContour(std::vector<cv::Point_<Tp>>& contour, double perimeter)
        {
                std::vector<cv::Point_<Tp>> approxPoly;
                cv::approxPolyDP(contour, approxPoly, perimeter*0.01, true);

                cv::convexHull(approxPoly, contour, false);

                /**
                try{ cv::convexHull( contour,contour, false ); }
                catch (cv::Exception & e)
                {
                        const char* err_msg = e.what();
                        std::cout << "convex hull skipped. Large contour." << err_msg << std::endl;
                }*/
        }

        template<typename Tp>
        inline bool computeIntersect(const std::array<cv::Point_<Tp>,2>& L1,
                                     const std::array<cv::Point_<Tp>,2>& L2,
                                     cv::Point_<Tp>& pt)
        {
                const Tp &x1 = L1[0].x, &y1 = L1[0].y;
                const Tp &x2 = L1[1].x, &y2 = L1[1].y;

                const Tp &x3 = L2[0].x, &y3 = L2[0].y;
                const Tp &x4 = L2[1].x, &y4 = L2[1].y;

                if (Tp d = ((x1-x2) * (y3-y4)) - ((y1-y2) * (x3-x4)))
                {
                        pt.x = ((x1*y2 - y1*x2) * (x3-x4) - (x1-x2) * (x3*y4 - y3*x4)) / d;
                        pt.y = ((x1*y2 - y1*x2) * (y3-y4) - (y1-y2) * (x3*y4 - y3*x4)) / d;

                        return true;
                }

                return false;
        }


        template<typename Tp>
        inline bool makeQuadrilateral( std::vector<cv::Point_<Tp>>& contour )
        {
                const size_t s = contour.size();

                std::vector<cv::Point_<Tp>>         dstContour( 4 );
                std::vector<double>                 lengths( s-1 );

                std::vector< std::array< cv::Point_<Tp>, 2 > > lineCoords( s-1 );

                for (size_t i=0; i < s - 1; ++i)
                {
                        lengths[i] = std::abs(contour[i+1].x - contour[i].x) +
                                     std::abs(contour[i+1].y - contour[i].y);  //taxicab distance or l1-norm

                        lineCoords[i] = { contour[i+1], contour[i] };
                }

                std::vector<int> I_full = ordered( lengths );               // find the ordered indices of the norms of the edges (lines)

                std::vector<int> I( I_full.begin(), I_full.begin() + 4 );   // need the first four elements of the ordered vector

                std::sort(I.begin(), I.end());                              // ordered because we need consecutive lines

                // find interesections of lines
                for (size_t i=0; i < 4; ++i)
                {
                        if ( !computeIntersect( lineCoords[ I[i] ], lineCoords[ I[(i+1)%4] ], dstContour[i] ) )
                        {
                                return false;
                        }
                }

                // further filtering in case of concavity or a large/small new contour (parrallel edges)
                if (!cv::isContourConvex(dstContour) ||
                    std::abs(cv::contourArea(dstContour)) > 1.2*std::abs(cv::contourArea(contour) ) ||
                    std::abs(cv::contourArea(dstContour)) < 0.5*std::abs(cv::contourArea(contour) ) ||
                    std::abs(cv::arcLength(dstContour, true)) > 1.2*std::abs(cv::arcLength(contour, true) ) ||
                    std::abs(cv::arcLength(dstContour, true)) < 0.5*std::abs(cv::arcLength(contour, true) ) //||
                    //std::abs(cv::arcLength(dstContour, true)) < 4*MIN_TAG_SIZE ||
                    //std::abs(cv::contourArea(dstContour)) < MIN_TAG_AREA
                   )
                {
                        return false;
                }

                contour = dstContour;

                return true;
        }

        template<typename Tp>
        bool inline contourSelection(std::vector<cv::Point_<Tp>>& contour, bool quadrilateralReforming, double pyramidScale)
        {
                double perimeter = std::abs(cv::arcLength(contour, true));
                double area = std::abs(cv::contourArea(contour));

                if (perimeter > 4.0*MIN_TAG_SIZE && area > MIN_TAG_AREA)
                {
                        normaliseContour(contour, perimeter);

                        size_t edges = contour.size();

                        if (edges == 4)
                        {
                                // we like quadrilaterals!
                                return true;
                        }
                        else if (quadrilateralReforming && pyramidScale <= 4 && edges > 4 && edges < 8) // not quadrilaterals, but maybe very close to ones
                        {
                                // try to transform the contour into a quadriteral by finding the intersections
                                // of the major four sides
                                if (makeQuadrilateral(contour))
                                {
                                        return true;
                                }
                        }
                }

                return false;
        }
}


























