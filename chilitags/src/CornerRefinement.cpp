#include "CornerRefinement.hpp"

namespace chilitags
{
        namespace redetection
        {

        inline bool FeatureDetector::check_and_extract_point( Keypoints& keypoints, cv::Point2f& resulting_point)
        {

                if( !keypoints.empty() )
                {
                        std::sort(keypoints.begin(), keypoints.end(), response_comparator);

                        resulting_point = keypoints[0].pt; //+ cRoioffset - roiOffset;

                        return true;
                }
                else
                {
                        return false;
                }

        }

        inline bool FeatureDetector::try_refine_corners( const cv::Mat& inputImage,
                                                         cv::Mat_<cv::Point2f>& points,
                                                         const cv::Size& neighborhood_size )
        {
                try
                {

                    cv::cornerSubPix( inputImage,
                                      points,
                                      neighborhood_size,
                                      cv::Size(-1, -1),
                                      cv::TermCriteria( cv::TermCriteria::MAX_ITER + cv::TermCriteria::EPS, 5, 0.01f) );
                }
                catch( const cv::Exception& e )
                {

                        std::cerr << "CORNERSUBPIX : " << e.what() << std::endl;
                        return false;

                }

                return true;
        }




        bool FeatureDetector::refine(const cv::Mat &inputImage, cv::Mat_<cv::Point2f>& points)
        {
                if( !m_refine_corners ){ return true; }

                return try_refine_corners( inputImage, points,
                        cv::Size( static_cast<int>(m_refine_neighborhood), static_cast<int>(m_refine_neighborhood)) );

        }

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------

        void FeatureDetector::set_refine_neighborhood( float proximity_ratio, float neighborhood_size )
        {

                m_refine_neighborhood = neighborhood_size;
        }

        void ORBDetector::set_refine_neighborhood( float proximity_ratio, float neighborhood_size )
        {
                float averageTagSide = neighborhood_size / proximity_ratio;
                m_refine_neighborhood =  cv::max(0.06f * averageTagSide, 1.0f);
        }

        void FASTDetector::set_refine_neighborhood( float proximity_ratio, float neighborhood_size )
        {

                float averageTagSide = neighborhood_size / proximity_ratio;
                m_refine_neighborhood =  cv::max(0.06f * averageTagSide, 1.0f);
        }

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------

        bool FASTDetector::detect( const cv::Mat& inputImage, cv::Point2f& resulting_point)
        {
                Keypoints keypoints;

                try{
                        cv::FAST( inputImage, keypoints, 50 );
                }
                catch( cv::Exception& e)
                {

                  std::cerr << "FAST : " << e.what() << std::endl;
                  return false;

                }

                return check_and_extract_point(keypoints, resulting_point);

        }

        ORBDetector::ORBDetector(bool refine_corners, int n_features, float scale_factor, int n_levels,
                                 int edge_threshold, int first_level, int WTA_K, int score_type, int patch_size)
                : FeatureDetector(refine_corners)
#if CV_VERSION_EPOCH == 2
                , m_orb(new cv::ORB(n_features, scale_factor, n_levels, edge_threshold, first_level, WTA_K,
                                    score_type, patch_size))
#else
                , m_orb(cv::ORB::create(n_features, scale_factor, n_levels, edge_threshold, first_level, WTA_K,
                                        score_type, patch_size))
#endif
        {}

        bool ORBDetector::detect( const cv::Mat& inputImage, cv::Point2f& resulting_point)
        {

                Keypoints keypoints;

                try{
#if CV_VERSION_EPOCH == 2
                        (*m_orb)(inputImage, cv::Mat(), keypoints);
#else
                        m_orb->compute(inputImage, keypoints, cv::Mat());
#endif
                }
                catch( cv::Exception& e )
                {
                        std::cerr << "ORB : " << e.what() << std::endl;
                        return false;
                }

                return check_and_extract_point( keypoints, resulting_point);
        }

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------


        }

}
