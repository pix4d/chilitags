# Build type
set(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "Configs" FORCE)
if(DEFINED CMAKE_BUILD_TYPE)
   SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS ${CMAKE_CONFIGURATION_TYPES})
endif()

if (NOT CMAKE_BUILD_TYPE)
        message( WARNING "Release build mode selected by default")
        set(CMAKE_BUILD_TYPE "Release")
endif()

# ----------------------------------------------------------------------------
#   PROJECT CONFIGURATION
if (UNIX)
set(EXTERNAL_DIR           "${CMAKE_SOURCE_DIR}/../../external" CACHE PATH "Path with 3rd libs and headers")
else(WIN32)
set(EXTERNAL_DIR           "${CMAKE_SOURCE_DIR}/../external" CACHE PATH "Path with 3rd libs and headers")
endif()

option(OPENCV_SUPPORT           "Build specific components depending on OpenCV"                         OFF)
option(WARNINGS_ARE_ERRORS      "Treat warnings as errors"                                              OFF)
option(WARNINGS_ANSI_ISO        "Issue all the mandatory diagnostics Listed in C standard"              OFF)

set(CMAKE_INCLUDE_DIRS_CONFIGCMAKE "${CMAKE_INSTALL_PREFIX}/include")
set(CMAKE_LIB_DIRS_CONFIGCMAKE     "${CMAKE_INSTALL_PREFIX}/lib")
