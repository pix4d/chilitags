macro(remove_c_flag flag)
    string(REPLACE "${flag}" "" CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
endmacro()

set(EXTRA_C_FLAGS "")
set(EXTRA_C_FLAGS_RELEASE "")
set(EXTRA_C_FLAGS_DEBUG "")
set(EXTRA_EXE_LINKER_FLAGS "")
set(EXTRA_EXE_LINKER_FLAGS_RELEASE "")
set(EXTRA_EXE_LINKER_FLAGS_DEBUG "")

#Overwrite default flags
if (UNIX)
        set (CMAKE_C_FLAGS_RELEASE          "-O3")
        set (CMAKE_CXX_FLAGS                "")
        set (CMAKE_CXX_FLAGS_DEBUG          "-g")
        set (CMAKE_CXX_FLAGS_RELEASE        "-O3")
elseif (MSVC)
        set (CMAKE_CXX_FLAGS                "/DWIN32 /D_WINDOWS /GR /EHa")
        set (CMAKE_CXX_FLAGS_DEBUG          "/MDd /Zi /Ob0 /Od /RTC1 /D_DEBUG")
        set (CMAKE_CXX_FLAGS_RELEASE        "/MD /O2 /Ob2 /D NDEBUG")
        set (CMAKE_C_FLAGS ${CMAKE_CXX_FLAGS})
        set (CMAKE_C_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE})
        set (CMAKE_C_FLAGS_DEBUG ${CMAKE_CXX_FLAGS_DEBUG})

        set (CMAKE_EXE_LINKER_FLAGS          "/machine:x64")
        set (CMAKE_EXE_LINKER_FLAGS_RELEASE  "")
        set (CMAKE_EXE_LINKER_FLAGS_DEBUG    "/debug")
endif()

if (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")

        # TODO - Remove extra from here
        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wall -Wextra -std=c++11 -Wno-deprecated-declarations -pthread")

        if (APPLE)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -isystem /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1 -framework Accelerate -framework Carbon -framework CoreFoundation -framework Foundation -framework Cocoa -framework OpenGL -framework GLUT -framework IOKit")
                set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS} -stdlib=libc++")
        endif()

        set(EXTRA_C_FLAGS_RELEASE "-DNDEBUG ${EXTRA_C_FLAGS_RELEASE}")

        # linking: whole program optimizations & remove unused functions (problems with CUDA)
#        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -flto")
#        set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS} -flto")

        # strip the release executable (remove symbols)
        if (NOT USE_PROFILING AND NOT PRODUCE_DEBUG_INFO AND NOT APPLE)
                set(EXTRA_EXE_LINKER_FLAGS_RELEASE "${EXTRA_EXE_LINKER_FLAGS_RELEASE} -s")
        endif()

        if(WARNINGS_ANSI_ISO)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -pedantic")
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-unknown-pragmas")
                # Extra memory checks on debug
                set(EXTRA_C_FLAGS_DEBUG "${EXTRA_C_FLAGS_DEBUG} -lmcheck")
        else()
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-attributes -Wno-unused-parameter")
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-unused-local-typedefs -Wno-maybe-uninitialized")
                if (NOT BUILD_ANDROID)
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -mpopcnt -msse4 -mfpmath=sse")
                endif()
        endif()

        if(WARNINGS_ARE_ERRORS)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Werror")
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Werror=return-type")
        endif()

        if(${CMAKE_BUILD_TYPE} STREQUAL Release AND PRODUCE_DEBUG_INFO)
                set(EXTRA_C_FLAGS_RELEASE "-g ${EXTRA_C_FLAGS_RELEASE}")
        endif()


endif()

if (${CMAKE_CXX_COMPILER_ID} STREQUAL GNU)
#        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fvisibility=hidden") #Useful for dynamic libraries

        if (NOT USE_DISTCC AND NOT BUILD_ANDROID) # march=native and mtune=native dont work with distcc or cross-compilation
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -mtune=native")
        endif()

        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wnon-virtual-dtor")

        execute_process(
        COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
        if (NOT (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7))
                message(FATAL_ERROR "${PROJECT_NAME} c++11 support requires g++ 4.7 or greater.")
        endif ()

        if (GCC_VERSION VERSION_GREATER 4.8 OR GCC_VERSION VERSION_EQUAL 4.8)
                if (USE_DEBUG_OPTIMIZATIONS)
                        set(EXTRA_C_FLAGS_DEBUG "${EXTRA_C_FLAGS_DEBUG} -Og")
                else()
                        set(EXTRA_C_FLAGS_DEBUG "${EXTRA_C_FLAGS_DEBUG} -O0")
                endif()
                # Disable macro expansion, it is slow and uses up to 50% more memory
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -ftrack-macro-expansion=0 -ftemplate-backtrace-limit=0")
                if(USE_TSAN)
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=thread")
                endif()
                if(USE_ASAN)
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=address -fno-omit-frame-pointer")
                endif()
        else()
                set(EXTRA_C_FLAGS_DEBUG "${EXTRA_C_FLAGS_DEBUG} -O0")
        endif ()

        if (GCC_VERSION VERSION_GREATER 4.9 OR GCC_VERSION VERSION_EQUAL 4.9)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fdiagnostics-color=always")
                if(USE_ASAN)
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=leak")
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=integer-divide-by-zero")
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=unreachable")
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=null")
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=return")
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=signed-integer-overflow")
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} --param=max-vartrack-size=0")
                endif()
                if(USE_USAN)
                        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=undefined")
                endif()
        endif()

        if(WARNINGS_ANSI_ISO)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wstrict-aliasing=3")
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wreturn-type")
        else()
#                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-narrowing -Wno-delete-non-virtual-dtor")
        endif()

        if(USE_PROFILING)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -pg")
                # turn off incompatible options
                foreach(flags   EXTRA_C_FLAGS
                                EXTRA_C_FLAGS_RELEASE
                                EXTRA_C_FLAGS_DEBUG)
                        string(REPLACE "-fomit-frame-pointer" "" ${flags} "${${flags}}")
                        string(REPLACE "-ffunction-sections" "" ${flags} "${${flags}}")
                endforeach()
        elseif(NOT APPLE AND NOT ANDROID)
                # Remove unreferenced functions: function level linking
                #set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -ffunction-sections")
        endif()

        if(ENABLE_COVERAGE OR ENABLE_COVERAGE_XML)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} --coverage")
        endif()


elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL Clang)

        if(NOT EXISTS ${CMAKE_CXX_COMPILER})
                message( FATAL_ERROR "Clang++ not found. " )
        endif()

        if(ENABLE_COVERAGE OR ENABLE_COVERAGE_XML)
                message(FATAL_ERROR "Not use clang for generate code coverage. Use gcc. ")
        endif()

        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -ftemplate-depth=512") # required for building some boost-code with clang
        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fcolor-diagnostics")  # color output for clang, even if piped to less
        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-deprecated-register -Qunused-arguments -Wno-unknown-warning-option")
        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-unused-const-variable")

        # Disable warnings caused by the combination of QT macros, clang 3.6, and (for the self-assign warning) ccache.
        # Use with care, as it may hide actual issues.
        # set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -Wno-self-assign -Wno-inconsistent-missing-override")

        if(USE_TSAN)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=thread")
        endif()
        if(USE_ASAN)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} -fsanitize=address -fno-omit-frame-pointer")
        endif()

elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)

        if(NOT PLATFORM MATCHES "64")
                message(FATAL_ERROR "Windows tool chain requires MSVC x64 compiler")
        endif()

        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /wd4503")
        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /bigobj") # Increases number of sections that an object file can contain
        set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /MP") # Multi-threaded compilations.
        #It applies to compilations, bot not to linking or link-time code gen.

        #set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS} /time /verbose:incr") # Debug linking times
        set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS} /ignore:4049 /ignore:4217 /ignore:4099")
        set(EXTRA_EXE_LINKER_FLAGS_RELEASE "${EXTRA_EXE_LINKER_FLAGS_RELEASE} /NODEFAULTLIB:libcmt")
        set(EXTRA_EXE_LINKER_FLAGS_DEBUG "${EXTRA_EXE_LINKER_FLAGS_DEBUG} /OPT:NOREF /OPT:NOICF /NODEFAULTLIB:MSVCRT")

        if (PRODUCE_BETTER_EXES)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /Gy") # package individual funcions in packaged functions (COMDATs)
                # This is needed for /OPT. /ZI implies /Gy, but we are not using /ZI

                # Produces PDBs & it implies /DEBUG, but it doesn't affect optimizations
                set(EXTRA_C_FLAGS_RELEASE "${EXTRA_C_FLAGS_RELEASE} /Zi")
                set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS} /INCREMENTAL:NO")
                set(EXTRA_EXE_LINKER_FLAGS_RELEASE "${EXTRA_EXE_LINKER_FLAGS_RELEASE} /DEBUG") # TODO - probably can be removed
                set(EXTRA_EXE_LINKER_FLAGS_RELEASE "${EXTRA_EXE_LINKER_FLAGS_RELEASE} /OPT:REF /OPT:ICF")
                #Remove unreferenced functions: function level linking
        else()
                #It's better to disable the better EXES for the development process (faster compilations)
                set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS} /INCREMENTAL")
        endif()

        if(WARNINGS_ANSI_ISO)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /W4")
        else()
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /W3")
        endif()

        if(WARNINGS_ARE_ERRORS)
                set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS} /WX")
        endif()

else()

        message(FATAL_ERROR "${CMAKE_CXX_COMPILER_ID} compiler not supported in this project.")

endif()

if (USE_LD_GOLD)
    execute_process(COMMAND ${CMAKE_C_COMPILER} -fuse-ld=gold -Wl,--version ERROR_QUIET OUTPUT_VARIABLE LD_VERSION)
    if ("${LD_VERSION}" MATCHES "GNU gold")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=gold -Wl,--disable-new-dtags")
        set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=gold -Wl,--disable-new-dtags")
    else ()
        message(WARNING "GNU gold linker isn't available, using the default system linker.")
    endif ()
endif ()

if (USE_DEBUG_FISSION)
    if (NOT USE_LD_GOLD)
        message(FATAL_ERROR "Need GNU gold linker for Debug Fission support")
    endif ()
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -gsplit-dwarf")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -gsplit-dwarf")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--gdb-index")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--gdb-index")
endif ()

# ==========================================================
# Add user supplied extra options (optimization, etc...)
# ==========================================================
set(EXTRA_C_FLAGS "${EXTRA_C_FLAGS}" CACHE INTERNAL "Extra compiler options")
set(EXTRA_C_FLAGS_RELEASE "${EXTRA_C_FLAGS_RELEASE}" CACHE INTERNAL "Extra compiler options for Release build")
set(EXTRA_C_FLAGS_DEBUG "${EXTRA_C_FLAGS_DEBUG}" CACHE INTERNAL "Extra compiler options for Debug build")
set(EXTRA_EXE_LINKER_FLAGS "${EXTRA_EXE_LINKER_FLAGS}" CACHE INTERNAL "Extra linker flags")
set(EXTRA_EXE_LINKER_FLAGS_RELEASE "${EXTRA_EXE_LINKER_FLAGS_RELEASE}" CACHE INTERNAL "Extra linker flags for Release build")
set(EXTRA_EXE_LINKER_FLAGS_DEBUG "${EXTRA_EXE_LINKER_FLAGS_DEBUG}" CACHE INTERNAL "Extra linker flags for Debug build")

#combine all "extra" options
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${EXTRA_C_FLAGS}")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} ${EXTRA_C_FLAGS_DEBUG}")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${EXTRA_C_FLAGS_RELEASE}")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${EXTRA_C_FLAGS}")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${EXTRA_C_FLAGS_RELEASE}")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${EXTRA_C_FLAGS_DEBUG}")

set(CMAKE_EXE_LINKER_FLAGS         "${CMAKE_EXE_LINKER_FLAGS} ${EXTRA_EXE_LINKER_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${EXTRA_EXE_LINKER_FLAGS_RELEASE}")
set(CMAKE_EXE_LINKER_FLAGS_DEBUG   "${CMAKE_EXE_LINKER_FLAGS_DEBUG} ${EXTRA_EXE_LINKER_FLAGS_DEBUG}")

#Remove from C compiler flags the specific flags for C++
if (UNIX)
        remove_c_flag("-std=c++11")
        remove_c_flag("-ftemplate-backtrace-limit=0")
        remove_c_flag("-Wnon-virtual-dtor")
        if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
                set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-long-long")
        endif()
endif()

