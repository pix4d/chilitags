# Windows definitions
if (WIN32)

        # \todo check if we still need this in msvc2015
        add_definitions(-DQ_COMPILER_INITIALIZER_LISTS)

        add_definitions(-D_WINSOCK_DEPRECATED_NO_WARNINGS)
        add_definitions(
                -DNOMINMAX
                -D_CRT_SECURE_NO_WARNINGS
                -D_SCL_SECURE_NO_WARNINGS
                -D_USE_MATH_DEFINES
                -DUNICODE
                -D_UNICODE
        )

        # Reduce build times
        add_definitions(-DWIN32_LEAN_AND_MEAN)  # exclude APIs such as Cryptography, DDE, RPC, Shell and Sockets
        # exclude many other APIS
        add_definitions(-DNOCOMM -DNOWINOFFSETS -DNOOPENFILE -DNOSERVICE -DNOSOUND -DNOATOM)
        add_definitions(-DNOWH -DNOKANJI -DNOHELP -DNOPROFILER -DNOMCX)

        # Support at least Windows Vista
        add_definitions(-D_WIN32_WINNT=0x0600 -DPSAPI_VERSION=1)
endif()

# Set a clang variable (right now it's only useful for the mobile application)
if (CMAKE_CXX_COMPILER MATCHES "^.*clang\\+\\+$")
        set(CLANG_BUILD True)
endif()

# enable use of "old" C++ abi with gcc 5.1 and newer
# (where "old" refers to the abi used by gcc 4.x)
if (USE_GCC5_OLD_ABI AND (${CMAKE_CXX_COMPILER_ID} STREQUAL GNU))
        execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
        if (GCC_VERSION VERSION_GREATER 5.0 OR GCC_VERSION VERSION_EQUAL 5.0)
                add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)
        endif()
endif()
# also support using "old" C++ abi with clang
if (USE_GCC5_OLD_ABI AND (${CMAKE_CXX_COMPILER_ID} STREQUAL Clang))
        add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)
endif()

# Platform
if(CMAKE_SIZEOF_VOID_P MATCHES "8")
        set(PLATFORM "64")
else()
        set(PLATFORM "32")
endif()
