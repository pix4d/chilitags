###########################################
# Configuring paths properly
###########################################
if (NOT EXTERNAL_DIR)
    message(FATAL_ERROR "EXTERNAL_DIR variable is not set")
endif()
list(APPEND CMAKE_PREFIX_PATH "${EXTERNAL_DIR}")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${EXTERNAL_DIR})

###########################################
# Setup OpenCV
###########################################

#find_package(OpenCV 3.1.0 QUIET REQUIRED core imgproc video features2d calib3d)

set(opencv_packages core imgproc video features2d calib3d)
message(STATUS "cv packages:  ${opencv_packages}")

foreach(pkg ${opencv_packages})
    unset(OPENCV_LIB CACHE)#unset to be able to find the next package
    find_library(OPENCV_LIB NAMES opencv_${pkg} opencv_${pkg}310 PATH_SUFFIXES lib)

    if(NOT OPENCV_LIB)
        message(FATAL_ERROR "Target opencv_${pkg} not found")
    endif()

    if(WIN32)
        unset(OPENCV_LIB_DBG CACHE)#unset to be able to find the next package
        find_library(OPENCV_LIB_DBG NAMES opencv_${pkg}${CMAKE_DEBUG_POSTFIX} opencv_${pkg}310${CMAKE_DEBUG_POSTFIX} PATH_SUFFIXES LIB)
        if(NOT OPENCV_LIB_DBG)
            message(FATAL_ERROR "Cannot find opencv debug library for ${pkg}")
        endif()
        list(APPEND OpenCV_LIBS optimized;${OPENCV_LIB} debug;${OPENCV_LIB_DBG})
    else()
        list(APPEND OpenCV_LIBS ${OPENCV_LIB})
    endif()
    message(STATUS "Found opencv package: ${pkg} at ${OPENCV_LIB}")
endforeach()

message(STATUS)
message(STATUS "OpenCV_LIBS: ${OpenCV_LIBS}")
